import Route from '@ember/routing/route';

export default Route.extend({
	model(){
		return this.get("store").findAll("reports");
	},

	actions:{
		clickMeToComplain:function(){
			this.transitionTo("complaint")
		},
		clickMeToMap:function(){
			this.transitionTo("complaint")
		}
	}
});
